#! /usr/bin/env node

const factory = require('../lib/laterbase');

var laterbase;

try {
    laterbase = factory.get();
} catch (e) {
    console.error(e.getMessage());
    process.exit();
}

var args = process.argv.slice(2);
var action = args.shift();

if (!laterbase.hasAction(action)) {
    console.log('Unknown action');
    process.exit();
}

laterbase.action(action, args);
