#! /usr/bin/env node

const nodeGetopt = require('node-getopt');
const factory = require('../lib/laterbase');

var laterbase;

try {
    laterbase = factory.get();
} catch (e) {
    console.error(e.message);
    process.exit();
}

var isTTY = Boolean(process.stdin.isTTY);

var help =
    "Usage:" +
    "\n  laterbase [name] <task>" +
    "\n  laterbase [name] < <filename>" +
    "\n  echo <task> | laterbase [name]" +
    "\n\nOptions:" +
    "\n[[OPTIONS]]\n";

var opts = laterbase.getOpts().concat([
    ['h', 'help', 'Displays this help']
]);

var getopt = nodeGetopt.create(opts).setHelp(help).bindHelp();
var opts = getopt.parseSystem();
var args = opts.argv;

function usage() {
    getopt.showHelp();
    process.exit();
}

if (isTTY) {
    if ((args.length != 1) && (args.length != 2)) {
        usage();
    }

    var task = args.pop();
    var name = args.pop();

    try {
        laterbase.send(task, name, opts.options);
    } catch(e) {
        console.error(e.message);
    }

    return;
}

if (args.length > 1) {
    usage();
}

var task = '';
var name = args.pop();

process.stdin.setEncoding('utf8');
process.stdin.resume();

process.stdin.on('data', function(data) {
    task += data;
});

process.stdin.on('end', function(data) {
    if (task) {
        try {
            laterbase.send(task, name, opts.options);
        } catch(e) {
            console.error(e.message);
        }
    } else {
        console.error('Nothing to do later !');
    }
});
