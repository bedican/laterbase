#! /usr/bin/env node

const store = require('../lib/config');
const services = require('../lib/services/services');

var args = process.argv.slice(2);
var service = args.shift();

var config;
if (store.exists()) {
    config = store.get();
} else {
    config = {};
}

if (!service) {
    if ((config.service) && (services[config.service])) {
        console.log('Currently: ' + config.service);
    } else {
        console.log('Please provide a service to switch to.');
    }

    process.exit();
}

if (!services[service]) {
    console.log('Unknown service');
    process.exit();
}

config.service = service;
store.put(config);

console.info('Service switched to ' + service);
