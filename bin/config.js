#! /usr/bin/env node

const store = require('../lib/config');
const services = require('../lib/services/services');

var args = process.argv.slice(2);

function usage() {
    console.info('Usage:');
    console.info('  laterbase-config <name> [value]');
    process.exit();
}

if ((args.length != 1) && (args.length != 2)) {
    usage();
}

var name = args.shift();
var value = args.shift();

var config = false;
if (store.exists()) {
    config = store.get();
}

if ((!config) || (!config.service) || (!services[config.service])) {
    console.info('No service configured, please use laterbase-service to set one.');
    process.exit();
}

config.services = config.services || {};
config.services[config.service] = config.services[config.service] || {};

if (!value) {
    if (config.services[config.service][name]) {
        console.info(name + ': ' + config.services[config.service][name]);
    } else {
        console.info('No configuration set for: ' + name);
    }

    process.exit();
}

config.services[config.service][name] = value;
store.put(config);

console.info('Config updated for: ' + name);
