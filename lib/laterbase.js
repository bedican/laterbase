const configFactory = require('./config');
const services = require('./services/services');

var Laterbase = function(service) {
    this.service = service;
};

Laterbase.prototype.getOpts = function() {
    return this.service.getOpts();
};

Laterbase.prototype.hasAction = function(action) {
    return ((action != 'send') && (this.service[action])) ? true: false;
};

Laterbase.prototype.action = function(action, args) {
    if (!this.hasAction(action)) {
        return;
    }
    this.service[action](args, function(message) {
        if (message) console.log(message);
    });
};

Laterbase.prototype.send = function(task, name, opts) {
    this.service.send(task, name, opts, function(message) {
        console.log(message || 'Added to the laterbase !');
    });
};

module.exports = {
    get: function() {

        var config = configFactory.get();

        if ((!config.service) || (!services[config.service])) {
            throw new Error('No service configured, please use laterbase-service to set one.');
        }

        var Service = services[config.service];
        var c = config.services[config.service] || {};

        var s = new Service(c);

        return new Laterbase(s);
    }
};
