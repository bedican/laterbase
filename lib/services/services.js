module.exports = {
    trello: require('./trello'),
    gist: require('./gist'),
    s3: require('./s3'),
};
