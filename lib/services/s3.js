var AWS = require('aws-sdk');
const dateformat = require('dateformat');

var S3 = function(config) {
    this.config = config;
};

S3.prototype.getOpts = function() {
    return [];
};

S3.prototype.send = function(task, name, opts, done) {

    if (!this.config.key) {
        throw new Error('Missing service configuration "key"');
    }
    if (!this.config.secret) {
        throw new Error('Missing service configuration "secret"');
    }
    if (!this.config.region) {
        throw new Error('Missing service configuration "region"');
    }
    if (!this.config.bucket) {
        throw new Error('Missing service configuration "bucket"');
    }

    var key = name || dateformat(new Date(), 'yyyy-mm-dd/hh-MM-ss');

    AWS.config.update({
        accessKeyId: this.config.key,
        secretAccessKey: this.config.secret,
        region: this.config.region
    });

    var s3 = new AWS.S3({apiVersion: '2006-03-01'});

    var params = {
        Bucket: this.config.bucket,
        Key: key,
        Body: task
    };

    s3.putObject(params, function(err, data) {
        if (err) {
            done(err.message);
            return;
        }

        done();
    });
};

module.exports = S3;
