const request = require('request');
const dateformat = require('dateformat');

var Gist = function(config) {
    this.config = config;
};

Gist.prototype.getOpts = function() {
    return [];
};

Gist.prototype.send = function(task, name, opts, done) {

    if (!this.config.token) {
        throw new Error('Missing service configuration "token"');
    }

    var filename = name || dateformat(new Date(), 'dd-mm-yyyy-hh-MM-ss');

    var files = {};
    files[filename] = {
        'content': task
    };

    var json = {
        'description': 'new file',
        'public': false,
        'files': files
    };

    var options = {
        method: 'POST',
        url: 'https://api.github.com/gists',
        json: true,
        body: json,
        headers: {
            'Accept': 'application/vnd.github.v3+json',
            'Authorization': 'token ' + this.config.token,
            'User-Agent': 'bedican/laterbase',
            'Content-Type': 'application/json;charset=utf8'
        }
    };

    request(options, function(err, response, body) {
        if ((err) || (response.statusCode != 201)) {
            done(err ? err.message : 'Status code was not 201');
            return;
        }

        done("Added:\n" + body.files[filename].raw_url);
    });
};

module.exports = Gist;
