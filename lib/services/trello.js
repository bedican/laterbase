const NodeTrello = require('node-trello');
const Table = require('cli-table');
const colors = require('colors');
const dateformat = require('dateformat');

const MILLIS_IN_HOUR = 1000 * 60 * 60;

var Trello = function(config) {
    if (!config.key) {
        throw new Error('Missing service configuration "key"');
    }
    if (!config.token) {
        throw new Error('Missing service configuration "token"');
    }

    this.config = config;
    this.trello = new NodeTrello(config.key, config.token);
};

Trello.prototype.getOpts = function() {
    return [
        ['d', 'days=ARG', 'Due in ARG days from now'],
        ['H', 'hours=ARG', 'Due in ARG hours from now']
    ];
};

Trello.prototype.send = function(task, name, opts, done) {

    var name = name || dateformat(new Date(), 'dd-mm-yyyy-hh-MM-ss');

    var due = null;

    if (opts.days || opts.hours) {
        due = new Date();
        var ts = due.getTime();

        if(opts.days) {
            ts += (MILLIS_IN_HOUR * 24 * opts.days);
        }
        if (opts.hours) {
            ts += (MILLIS_IN_HOUR * opts.hours);
        }

        due.setTime(ts);
    }

    var data = {
        name: name,
        desc: task,
        idList: this.config.list,
        urlSource: null,
        due: due
    };

    this.trello.post('/1/cards', data, function(err, result) {
        done(err ? err.message : false);
    });
};

Trello.prototype.boards = function(args, done) {
    var username = args.shift();

    if (!username) {
        done('Missing username');
        return;
    }

    var data = {
        boards: 'open'
    };

    this.trello.get('/1/members/' + username, data, function(err, result) {
        if (err) {
            done(err.message);
            return;
        }

        var consoleWidth = process.stdout.getWindowSize()[0];

        var table = new Table({
            head: [colors.bold.cyan('Board Name'), colors.bold.cyan('Board Id')],
            colWidths: [
                parseInt((consoleWidth / 100) * 30),
                parseInt((consoleWidth / 100) * 65)
            ],
            truncate: false
        });

        for(key in result.boards) {
            table.push(
                [result.boards[key].name, result.boards[key].id]
            );
        }

        console.info(table.toString());

        done();
    });
};

Trello.prototype.lists = function(args, done) {
    var boardId = args.shift();

    if (!boardId) {
        done('Missing boardId');
        return;
    }

    var data = {
        lists: 'open'
    };

    this.trello.get('/1/boards/' + boardId, data, function(err, result) {
        if (err) {
            done(err.message);
            return;
        }

        var consoleWidth = process.stdout.getWindowSize()[0];

        var table = new Table({
            head: [colors.bold.cyan('List Name'), colors.bold.cyan('List Id')],
            colWidths: [
                parseInt((consoleWidth / 100) * 30),
                parseInt((consoleWidth / 100) * 65)
            ],
            truncate: false
        });

        for(key in result.lists) {
            table.push(
                [result.lists[key].name, result.lists[key].id]
            );
        }

        console.info(table.toString());

        done();
    });

    done();
};

module.exports = Trello;
