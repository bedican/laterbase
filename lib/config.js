const fs = require('fs');

const FILENAME = process.env.HOME + '/.config/laterbase.json';

module.exports =  {
    exists: function() {
        var stat;

        try {
            stat = fs.statSync(FILENAME);
        } catch(e) {
            return false;
        }

        if (!stat.isFile()) {
            throw new Error('Exists but not a file, manual intervention required: "' + FILENAME + '"');
        }

        return true;
    },
    get: function() {
        var config;

        if(!this.exists()) {
            throw new Error('Missing configuration "' + FILENAME + '"');
        }

        try {
            config = JSON.parse(fs.readFileSync(FILENAME, 'utf8'));
        } catch (e) {
            throw new Error('Failed to load configuration');
        }

        return config;
    },
    put: function(config) {
        fs.writeFileSync(FILENAME, JSON.stringify(config, null, "\t"));
    },
};
