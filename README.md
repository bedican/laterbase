# Laterbase

Have something to do later? Put it in the laterbase !

## Installation

``` bash
# Install the module globally
$ sudo npm -g install laterbase

# Switch to a service, see below
$ laterbase-service s3

# Configure the service, see below
$ laterbase-config key abc123
$ laterbase-config secret abc123
$ laterbase-config bucket my-laterbase
$ laterbase-config region eu-west-1
```

## Usage

``` bash
# Using laterbase
$ laterbase "Pick up eggs"

# With a name
$ laterbase "Shopping" "Buy beer and snacks"

# With a file
$ laterbase "Shopping" < shpping-list.txt

# Piping
$ echo "Sign time sheets, raise invoices" | laterbase "Work"

# Using service actions, see below
$ laterbase-action boards me

# Switching service to trello
$ laterbase-service trello

# Setting service configuration, see below
$ laterbase-config token abc123
```

## Configuration

Configuration is performed within the file `~/.config/laterbase.json` and differs depending on the service used as your laterbase.

```json
{
    "service": "trello",
    "services": {
    }
}
```

Service configuration can be set or retrieved with the `laterbase-config` command as follows.

``` bash
# Setting service configuration, see below
$ laterbase-config token abc123

# Reading service configuration.
$ laterbase-config token
```

## Services

The following services are available, each service may optionally define additional options and actions.

### Trello

#### Configuration

To configure trello as the laterbase service, a key and token is required, along with a list id.

- Generate a key and token, by logging in to trello and visiting https://trello.com/1/appKey/generate
- Obtain the list id, using the trello service actions as detailed below.

```json
{
    "service": "trello",
    "services": {
        "trello": {
            "key": "<app key>",
            "token": "<user token>",
            "list": "<list id>"
        }
    }
}
```

#### Options

The trello service allows the following options.

| Option | Description               | Example                 |
| ------ | ------------------------- | ----------------------- |
| -d     | Due in ARG days from now  | `laterbase -d 4 "Task"` |
| -H     | Due in ARG hours from now | `laterbase -H 1 "Task"` |

#### Actions

The trello service provides the following actions, and can be used with the `laterbase-action` command. These provide the actions needed to obtain the `list` configuration when using `laterbase`.

```bash
# Display boards
$ laterbase-action boards <username>

# Display lists for a board
$ laterbase-action lists <boardId>

# Set the list id configuration
$ laterbase-config list <listId>
```

### Gist

#### Configuration

To configure gist as the laterbase service, a token is required.

- Generate a personal access token, by logging in to github and visiting https://github.com/settings/tokens/new

```json
{
    "service": "gist",
    "services": {
        "gist": {
            "token": "<access token>"
        }
    }
}
```

#### Options

The gist service does not allow any options.

#### Actions

The gist service does not provide any actions.

### Amazon S3

#### Configuration

To configure Amazon S3 as the laterbase service, an IAM user is required, along with bucket and region.

- Create an S3 bucket within the aws console.
- Create an IAM user within the aws console, and grant putObject access to the bucket.

```json
{
    "service": "s3",
    "services": {
        "s3": {
            "key": "<IAM user key>",
            "secret": "<IAM user secret>",
            "bucket": "<bucket>",
            "region": "<region>"
        }
    }
}
```

#### Options

The s3 service does not allow any options.

#### Actions

The s3 service does not provide any actions.

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
